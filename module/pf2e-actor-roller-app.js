// The saving throws possible to display
export const SAVES = [
    {'name': 'Fortitude', 'id': 'fortitude'},
    {'name': 'Reflex', 'id': 'reflex'},
    {'name': 'Will', 'id': 'will'}
]

// The skills possible to display
export const SKILLS = [
    {'name': 'Perception', 'id': 'perception'},
    {'name': 'Acrobatics', 'id': 'acr'},
    {'name': 'Arcana', 'id': 'arc'},
    {'name': 'Athletics', 'id': 'ath'},
    {'name': 'Crafting', 'id': 'cra'},
    {'name': 'Deception', 'id': 'dec'},
    {'name': 'Diplomacy', 'id': 'dip'},
    {'name': 'Intimidation', 'id': 'itm'},
    {'name': 'Medicine', 'id': 'med'},
    {'name': 'Nature', 'id': 'nat'},
    {'name': 'Occultism', 'id': 'occ'},
    {'name': 'Performance', 'id': 'prf'},
    {'name': 'Religion', 'id': 'rel'},
    {'name': 'Society', 'id': 'soc'},
    {'name': 'Stealth', 'id': 'ste'},
    {'name': 'Survival', 'id': 'sur'},
    {'name': 'Thievery', 'id': 'thi'}
];

// Combined saves/skills
export const ALL = SAVES.concat(SKILLS);


export class PF2eActorRollerApp extends Application {    
    /**
     * The Actor Roller application window
     */
    constructor(options) {
        options['title'] = "Actor Roller";
        super(options);

        // When opened, gather and roll all data
        this.table_data = this._roll_all();
    }

    getData() {
        // Display all enabled skills/saves for enabled pcs
        const enabled = game.settings.get('pf2e-actor-roller', 'attribute-settings');
        const skills = SKILLS.filter(skill => enabled[skill.id] == 'checked');
        const saves = SAVES.filter(save => enabled[save.id] == 'checked');

        return {SKILLS: skills, SAVES: saves, pcs: this.table_data}
    }

    activateListeners(html) {
        super.activateListeners(html);

        // When clicking on a skill name, reroll that table row
        html.find('.pf2ar-skill-name.rollable').click(event => {
            const data = event.currentTarget.dataset;
            this._roll_skill_row({id: data.skill})
            this.render(this.table_data);
        });

        // When clicking on a pc iamge, reroll all checks for that pc
        html.find('.pf2ar-pc-img.rollable').click(event => {
            const data = event.currentTarget.dataset;
            this._roll_actor(data.actor);
            this.render(this.table_data);
        });

        // When clicking the die, reroll all rolls
        html.find('.pf2ar-roller.rollable').click(event => {
            this.table_data = this._roll_all();
            this.render(this.table_data);
        });
    }

    _roll_skill_row(skill) {
        this.table_data.forEach(actor => {
            const skill_results = this._roll_skill(skill, actor);

            actor.dcs[skill.id] = skill_results.dc;
            actor.rolls[skill.id] = skill_results.roll;
            actor.crits[skill.id] = skill_results.crit;
        });
        this._play_sfx()
    }

    _roll_actor(index) {
        const pc = this.table_data[index]
        ALL.forEach(skill => {
            const skill_results = this._roll_skill(skill, pc);

            pc.dcs[skill.id] = skill_results.dc;
            pc.rolls[skill.id] = skill_results.roll;
            pc.crits[skill.id] = skill_results.crit
        });
        pc.dcs['ac'] = game.actors.get(pc.id).data.data.attributes.ac.value;
        this._play_sfx()
    }

    _roll_all() {
        const pcs = this._get_actors().map(actor =>{ 
            // Trim names to a smaller size if too large
            let trimmed_name = actor.data.name.substr(0, 9);
            let data = {id: actor.id, name: trimmed_name, image: actor.data.img, dcs: {}, rolls: {}, crits: {}}
            ALL.forEach(skill => {
                const skill_result = this._roll_skill(skill, data);
    
                data.dcs[skill.id] = skill_result.dc;
                data.rolls[skill.id] = skill_result.roll;
                data.crits[skill.id] = skill_result.crit;
            });
            data.dcs['ac'] = actor.data.data.attributes.ac.value;
            
            return data;
        });

        this._play_sfx()
        return pcs
    }

    _roll_skill(skill, pc) {
        const actor = game.actors.get(pc.id).data.data
        let skill_val = actor.skills[skill.id];
        // Have to change where to look for perception and saves
        if (skill.id == 'perception')
            skill_val = actor.attributes.perception;
        else if (skill.id == 'fortitude' || skill.id == 'reflex' || skill.id == 'will')
            skill_val = actor.saves[skill.id]

        let roll = new Roll("1d20 + @mod", {mod: skill_val.value});
        roll.roll({async: false})

        return {dc: skill_val.value + 10, roll: roll.total, crit: this._get_roll_results(roll)};
    }

    _get_roll_results(roll) {
        let die_result = roll.terms[0].results[0].result
        if (die_result>= 20)
            return 'crit';
        else if (die_result <= 1)
            return 'fumble';
        return '';
    }

    _get_actors() {
        const visible_pcs = game.settings.get('pf2e-actor-roller', 'pc-settings');
        return game.actors.filter(actor => actor.data.type === "character" && this._show_actor(visible_pcs, actor.id) === "checked");
    }

    _show_actor(visible_pcs, actor_id) {
        if (actor_id in visible_pcs)
            return visible_pcs[actor_id].show;
        return "checked";
    }

    _play_sfx() {
        if (game.settings.get("pf2e-actor-roller", "play-die-sfx"))
            AudioHelper.play({src: "sounds/dice.wav", volume: 0.2, loop: false}, false);
    }
}