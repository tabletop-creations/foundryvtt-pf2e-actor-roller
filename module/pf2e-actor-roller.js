import {PF2eActorRollerApp} from './pf2e-actor-roller-app.js';
import {PF2eActorAttributeSettings, PF2eActorPCSettings} from './pf2e-actor-settings.js'

// Add our icon to the control bar
Hooks.on('renderSceneControls', (controls, html) => { 
    // This module is GM only
    if (game.user.isGM) {
        const actor_roller_btn = $(`
            <li class="scene-control pf2ear-scene-control" data-control="pf2e-actor-roller" title="PF2e Actor Roller">
                <i class="far fa-list-alt"></i>
            
                <ol class="control-tools">
                    <div id="PF2ActorRollerPopup" class="pf2e-actor-roller-popup" style="display: none;">
                    </div>
                </ol>
            </li>
        `);

        // On click, render our app
        html.append(actor_roller_btn);
        actor_roller_btn[0].addEventListener('click', ev => {
            if (ev.path[1].className == 'scene-control pf2ear-scene-control')
                new PF2eActorRollerApp({template: 'modules/pf2e-actor-roller/templates/pf2e-actor-roller-app.html'}).render(true);
        });
    }
});

// Register settings
Hooks.once('init', async function() {  
    game.settings.register("pf2e-actor-roller", "play-die-sfx", {
        name: "Play SFX",
        hint: "Play the die rolling SFX when rerolling values on sheet",
        scope: "world",
        config: true,
        restricted: true,
        type: Boolean,
        choices: {
            true: "On",
            false: "Off"
        },
        default: true
    });

    game.settings.registerMenu("pf2e-actor-roller", "attribute-settings-menu", {
        name: "Attribute Settings",
        label: "Configure",
        hint: "Show/Hide attributes from the table",
        scope: "world",
        config: true,
        restricted: true,
        type: PF2eActorAttributeSettings
    });

    game.settings.register("pf2e-actor-roller", "attribute-settings", {
        name: "Attribute Settings",
        hint: "Show/Hide attributes from the table",
        scope: "world",
        config: false,
        restricted: true,
        type: Object,
        default: {}
    });

    game.settings.registerMenu("pf2e-actor-roller", "pc-settings-menu", {
        name: "PC Settings",
        label: "Configure",
        hint: "Show/Hide PCs from the table",
        scope: "world",
        config: true,
        restricted: true,
        type: PF2eActorPCSettings
    });

    game.settings.register("pf2e-actor-roller", "pc-settings", {
        name: "PC Settings",
        hint: "Show/Hide PCs from the table",
        scope: "world",
        config: false,
        restricted: true,
        type: Object,
        default: {}
    });
});
