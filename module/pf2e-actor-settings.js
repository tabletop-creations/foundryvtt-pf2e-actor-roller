import {SKILLS, SAVES} from './pf2e-actor-roller-app.js'

export class PF2eActorAttributeSettings extends FormApplication {
    /**
     * A setting form to show which attributes to display in the roller
     */
    constructor(options) {
        super(options);
        const sts = game.settings.get('pf2e-actor-roller', 'attribute-settings');
        if (Object.keys(sts).length == 0) {
            this.settings = {}
            SKILLS.forEach(skill => {this.settings[skill.id] = 'checked';});
            SAVES.forEach(save => {this.settings[save.id] = 'checked';});
            game.settings.set('pf2e-actor-roller', 'attribute-settings', this.settings)
        } else 
            this.settings = sts;
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['form'],
            popOut: true,
            template: `modules/pf2e-actor-roller/templates/pf2e-actor-roller-attr-settings.html`,
            id: 'pf2e-attribute-settings',
            title: 'Attribute Settings',
          });
      }

    getData() {
        return {SKILLS, SAVES, settings: this.settings};
    }

    async _updateObject(event, formData) {
        Object.entries(formData).forEach(([attr, flag]) => {
            this.settings[attr] = flag ? 'checked' : '';
        });
        game.settings.set('pf2e-actor-roller', 'attribute-settings', this.settings);
    }
}

export class PF2eActorPCSettings extends FormApplication {
    /**
     * A setting form to show which attributes to display in the roller
     */
    constructor(options) {
        super(options);
        this.settings = game.settings.get('pf2e-actor-roller', 'pc-settings');
        
        const pcs = game.actors.filter(actor => actor.data.type === "character")
        pcs.forEach(pc => {
            // If this is a pc not currently saved, save it as visible
            if (!(pc.id in this.settings))
                this.settings[pc.id] = {name: pc.data.name, show: 'checked'};
        });
        game.settings.set('pf2e-actor-roller', 'pc-settings', this.settings);
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ['form'],
            popOut: true,
            template: `modules/pf2e-actor-roller/templates/pf2e-actor-roller-pc-settings.html`,
            id: 'pf2e-pc-settings',
            title: 'PC Settings',
          });
      }

    getData() {
        return {PCS: this.settings};
    }

    async _updateObject(event, formData) {
        Object.entries(formData).forEach(([pc, flag]) => {
            this.settings[pc].show = flag ? 'checked' : '';
        });
        game.settings.set('pf2e-actor-roller', 'pc-settings', this.settings);
    }
}
