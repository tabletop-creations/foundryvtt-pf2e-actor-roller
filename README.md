# PF2E Actor Roller

Adds a table to the nav bar on the left of the canvas for quickly and easily viewing DCs and Rolls for all skills/saves for your PCs. Which skills/saves/PCs show up in the table is editable in the settings menu. 

## Possible Bugs

If the table does not load for you, go to the settings and enter the 'Configure PCs' setting to re-initialize which PCs are visible.